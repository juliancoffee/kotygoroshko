# Purpose
Demo of game using fluent-rs for i18n.
Integration with Weblate is planned as well, but not yet done.

# Usage
`$ cargo run` to launch the game.

# Configuration
`config.ron` to specify your desired language

`assets/i18n/config.ron` to add new languages
