help =
    {"..."} Press Enter to move storyline.
    {"?.."} Answer by sending answers.
    {"?"}   Make choices by sending numbers.
    Praise the sun.

# test comments
sign = You see a sign.

come-closer = Come closer?

read-the-sign = Read?

sign-message =
    The sign says that king's son is taken away by horrible monster.
    Crown promises to fulfill any wish for a hero who will return his son alive.

officer = Near the sign you see an officer, he seems to be blind.

sign-up = Sing up?
    .yes = (1) Yes.
    .no = (2) No.

refuse = You refuse and live boring but long life.

end = The end. 

your-name = < Who wants to volounteer? Say your name.

your-pronoun = < Whom I am speaking with?
    .he = (1) With him.
    .she = (2) With her.
    .they = (3) With them.

your-role = < Who are they in the world of owners and owned?
    .cossack = 
        (1) { $pronoun ->
            [he] Cossack is him, owner of himself.
            [she] Cossack is her, owner of herself.
            *[they] Cossack are them, owner of themself.
        }
    .shlyakhta = 
        (2) { $pronoun ->
            [he] Son of shlyahkta is him, owner of good name.
            [she] Daughter of shlyakhta is her, owner of good name.
            *[they] Child of shlyakhta are them, owner of good name.
        }
    .kripack = 
        (3) { $pronoun ->
            [he] Kripack is him, owned by lord.
            [she] Kripack is her, owned by lord.
            *[they] Kripack are them, owned by lord.
        }

city-intro = The big city with big possibilities, what will we do.
    .korchma = (1) Go to korchma to get drinks, information and problems.
    .steppe = (2) Go to steppe, to look for trace of monster.

korchma-intro =
    You see people trying to forget their lives by drinking drinks
    and playing dice. Someone will probably beaten tonight.
    .listen = (1) Ask about monster.
    .dice = (2) Play dice.
    .quite = (3) Quit korchma.

ask-monster = You ask about case with king's son and see a man who joins you.

drinker =
    < Hello, what's your name, { $name }, huh?
    You ask about monster? I saw it, buy me a medovukha,
    so that my throat is in better condition for telling things.

coins-info =
    You { NUMBER($coins) ->
        [0] have no coins left.
        [one] have { $coins } coin.
        *[other] have { $coins } coins.
    }

medovukha-info = Medovukha costs 1 coin.

cant-afford = > I can't afford that.

buying-drink = > Thinking about it...
    .buy = (1) I will buy it, tell me what you know.
    .refuse = (2) Sorry man, can't afford that.

drunk-to-death =
    Drinker happily grabs cup of medovukha, chuggs it's all and falls
    on the floor.

sleeping = Z-z-z.

bad-witness = Now you have one witness less and one coin less.

drinker-sad =
    < I don't want to even have conversation with greedy people. Get out!

witness-chance = Your last witness is still not in good mood to answer questions.

your-empty-purse =
    Looking at your purse, you realize that you don't have any money.

playing-dice =
    Make your bet.
    If you win, you get double amount.
    If you lose, you lose your bet.

dice-rules =
    Rules are simple.
    Each player throws four dice.
    If you have multiple dice with same number on it, your score multiplies
    by that number.

your-results = Your results.
opponent-results = Opponent results.

score = { NUMBER($times) ->
    [one] One throw of { $num }.
    *[other] { $times } throws of { $num }.
}

total = { $points } points total.

you-won = { NUMBER($bet) ->
    [one] You won { $bet } coin.
    *[other] You won { $bet } coins.
}

you-lost = { NUMBER($bet) ->
    [one] You lost { $bet } coin.
    *[other] You lost { $bet } coins.
}

fear-of-unknown =
    You see a steppe before you, full of unknown possibilities.
    They say that oldest and strongest emotion of mankind is fear,
    and the oldest and strongest kind of fear is fear of the unknown.
    You can't handle this fear and die.
