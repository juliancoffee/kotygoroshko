pub use fluent_bundle::FluentArgs;

use fluent_bundle::{
    types::FluentNumber, FluentBundle, FluentMessage, FluentResource,
    FluentValue,
};

use unic_langid::{langid, LanguageIdentifier};

use std::{borrow::Cow, collections::HashMap, fs::File, io::Read};

use crate::config::{LangConfig, UserConfig};

pub struct Locale {
    langs: HashMap<LanguageIdentifier, FluentBundle<FluentResource>>,
    current_lang: LanguageIdentifier,
    fallback: LanguageIdentifier,
}

fn bundle(
    path: &str,
    lang: LanguageIdentifier,
) -> FluentBundle<FluentResource> {
    let mut src = File::open(path).expect("failed to open .ftl");
    let mut content = String::new();
    src.read_to_string(&mut content)
        .expect("failed to read .ftl");
    let resource = match FluentResource::try_new(content) {
        Ok(res) => res,
        Err((_, errs)) => panic!("{errs:?}"),
    };
    let mut bundle = FluentBundle::new(vec![lang]);
    bundle
        .add_function("NUMBER", |positional, named| match positional.get(0) {
            Some(FluentValue::Number(n)) => {
                let mut num = n.clone();
                num.options.merge(named);

                FluentValue::Number(num)
            }
            Some(FluentValue::String(s)) => {
                let num: f64 = if let Ok(n) = s.to_owned().parse() {
                    n
                } else {
                    return FluentValue::Error;
                };
                let mut num = FluentNumber {
                    value: num,
                    options: Default::default(),
                };
                num.options.merge(named);

                FluentValue::Number(num)
            }
            _ => FluentValue::Error,
        })
        .expect("Failed to add a function.");
    bundle
        .add_resource(resource)
        .expect("failed to bundle .ftl");

    bundle
}

struct Entry<'a> {
    bundle: &'a FluentBundle<FluentResource>,
    msg: FluentMessage<'a>,
}

type Args<'a> = FluentArgs<'a>;

impl Locale {
    pub fn new() -> Self {
        const REFERENCE: LanguageIdentifier = langid!("en-US");

        let user_config = UserConfig::load();
        let current_lang = user_config
            .desired_lang
            .parse()
            .expect("can't parse desired lang id");
        let lang_config = LangConfig::load();

        let mut langs = HashMap::new();
        langs.insert(REFERENCE, bundle("assets/i18n/en/dialog.ftl", REFERENCE));

        for (lang, bundle_file) in lang_config.langs {
            let id: LanguageIdentifier =
                lang.parse().expect("wrong language identifier");
            langs.insert(id.clone(), bundle(&bundle_file, id));
        }

        Self {
            langs,
            current_lang,
            fallback: REFERENCE,
        }
    }

    fn bundle(&self) -> &FluentBundle<FluentResource> {
        self.langs
            .get(&self.current_lang)
            .expect("failed to get bundle for current locale")
    }

    fn fallback(&self) -> &FluentBundle<FluentResource> {
        self.langs
            .get(&self.fallback)
            .expect("failed to get bundle for fallback locale")
    }

    fn get_entry(&self, key: &str) -> Option<Entry> {
        let first_bundle = self.bundle();
        let first_msg = first_bundle.get_message(key);
        if let Some(msg) = first_msg {
            Some(Entry {
                bundle: first_bundle,
                msg,
            })
        } else {
            let fallback = self.fallback();
            Some(Entry {
                bundle: fallback,
                msg: fallback.get_message(key)?,
            })
        }
    }

    // Generic way to get message with or without arguments
    pub fn try_msg_cow<'a>(
        &'a self,
        key: &str,
        args: Option<&'a FluentArgs>,
    ) -> Option<Cow<str>> {
        let Entry { bundle, msg } = self.get_entry(key)?;

        let mut errs = Vec::new();
        let msg = bundle.format_pattern(msg.value()?, args, &mut errs);
        for err in errs {
            eprintln!("err: {err}")
        }

        Some(msg)
    }

    pub fn try_msg(&self, key: &str) -> Option<Cow<str>> {
        self.try_msg_cow(key, None)
    }

    pub fn get_msg(&self, key: &str) -> Cow<str> {
        self.try_msg(key).unwrap_or_else(|| {
            panic!("no {key} message");
        })
    }

    pub fn try_msg_ctx<'a>(
        &'a self,
        key: &str,
        args: &'a Args,
    ) -> Option<Cow<'a, str>> {
        self.try_msg_cow(key, Some(args))
    }

    pub fn get_msg_ctx<'a>(
        &'a self,
        key: &str,
        args: &'a Args,
    ) -> Cow<'a, str> {
        self.try_msg_ctx(key, args).unwrap_or_else(|| {
            panic!("no {key} message");
        })
    }

    // Generic way to get attributes with or without arguments
    pub fn try_attrs_cow<'a>(
        &'a self,
        key: &str,
        args: Option<&'a Args>,
    ) -> Option<Vec<Cow<str>>> {
        let Entry { bundle, msg } = self.get_entry(key)?;

        let mut errs = Vec::new();
        let mut attrs = Vec::new();

        for attr in msg.attributes() {
            let msg = bundle.format_pattern(attr.value(), args, &mut errs);
            attrs.push(msg);
        }
        for err in errs {
            eprintln!("err: {err}")
        }

        Some(attrs)
    }

    pub fn try_attrs(&self, key: &str) -> Option<Vec<Cow<str>>> {
        self.try_attrs_cow(key, None)
    }

    pub fn get_attrs(&self, key: &str) -> Vec<Cow<str>> {
        self.try_attrs(key).unwrap_or_else(|| {
            panic!("no attrs for {key} message");
        })
    }

    pub fn try_attrs_ctx<'a>(
        &'a self,
        key: &str,
        args: &'a Args,
    ) -> Option<Vec<Cow<'a, str>>> {
        self.try_attrs_cow(key, Some(args))
    }

    pub fn get_attrs_ctx<'a>(
        &'a self,
        key: &str,
        args: &'a Args,
    ) -> Vec<Cow<'a, str>> {
        self.try_attrs_ctx(key, args).unwrap_or_else(|| {
            panic!("no attrs for {key} message");
        })
    }
}
