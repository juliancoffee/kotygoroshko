use serde::Deserialize;
use std::fs::File;

#[derive(Deserialize)]
pub struct UserConfig {
    pub desired_lang: String,
}

#[derive(Deserialize)]
pub struct LangConfig {
    pub langs: Vec<(String, String)>,
}

impl UserConfig {
    pub fn load() -> Self {
        let file = File::open("./config.ron").unwrap();

        ron::de::from_reader(file).unwrap()
    }
}

impl LangConfig {
    pub fn load() -> Self {
        let file = File::open("assets/i18n/config.ron").unwrap();
        ron::de::from_reader(file).unwrap()
    }
}
