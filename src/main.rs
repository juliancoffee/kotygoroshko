mod config;
mod game;
mod locale;

fn main() {
    let i18n = locale::Locale::new();

    game::game_loop(i18n);
}
