use crate::locale::{FluentArgs, Locale};
use fluent::fluent_args;
use std::{collections::HashMap, fmt, io, io::Write};

#[derive(Copy, Clone)]
enum Status {
    Cossack,
    Shlyakhta,
    Kripack,
}

#[derive(Copy, Clone)]
enum PronounKind {
    He,
    She,
    They,
}

impl fmt::Display for PronounKind {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let s = match self {
            Self::He => "he",
            Self::She => "she",
            Self::They => "they",
        };

        write!(f, "{}", s)
    }
}

#[derive(PartialEq, Eq)]
enum Event {
    DrunkToDeath,
}

struct Character {
    name: String,
    #[allow(unused)]
    status: Status,
    #[allow(unused)]
    pronoun: PronounKind,
    coins: u32,
    events: Vec<Event>,
}

fn get_line() -> String {
    let mut buff = String::new();
    io::stdin()
        .read_line(&mut buff)
        .expect("error: unable to read user input");

    buff
}

// Print localized message
fn show(
    i18n: &Locale,
    key: &str,
    args: Option<FluentArgs>,
    with_options: bool,
) {
    let msg = if let Some(args) = &args {
        i18n.get_msg_ctx(key, args)
    } else {
        i18n.get_msg(key)
    };
    println!("{}", msg);

    if with_options {
        let options = if let Some(args) = &args {
            i18n.get_attrs_ctx(key, args)
        } else {
            i18n.get_attrs(key)
        };
        for option in options {
            println!("{}", option)
        }
    }
}

// Show message and wait for Enter key
fn info(i18n: &Locale, key: &str, args: Option<FluentArgs>) {
    show(i18n, key, args, false);
    print!("...");
    io::stdout().flush().unwrap();
    let _ = get_line();
}

// Show message and wait for input
fn question(i18n: &Locale, key: &str, args: Option<FluentArgs>) -> String {
    show(i18n, key, args, false);
    print!("?.. ");
    io::stdout().flush().unwrap();

    get_line().trim().to_owned()
}

// Show message, options and return index of choice
fn choice(i18n: &Locale, key: &str, args: Option<FluentArgs>, n: u32) -> u32 {
    show(i18n, key, args, true);

    loop {
        print!("? ");
        io::stdout().flush().unwrap();
        let num = get_line().trim().parse();
        if let Ok(num) = num {
            if num > 0 && num <= n {
                break num;
            }
        }
        println!();
    }
}

fn intro(i18n: &Locale) -> Option<Character> {
    info(i18n, "help", None);
    info(i18n, "sign", None);
    info(i18n, "come-closer", None);
    info(i18n, "read-the-sign", None);
    info(i18n, "sign-message", None);
    info(i18n, "officer", None);

    // Sing up
    if choice(i18n, "sign-up", None, 2) == 2 {
        return None;
    }
    // Name
    let name = question(i18n, "your-name", None);
    // Pronoun
    let pronoun = match choice(i18n, "your-pronoun", None, 3) {
        1 => PronounKind::He,
        2 => PronounKind::She,
        3 => PronounKind::They,
        _ => unreachable!(),
    };
    // Status
    let status = match choice(
        i18n,
        "your-role",
        Some(fluent_args! {
            "pronoun" => pronoun.to_string()
        }),
        3,
    ) {
        1 => Status::Cossack,
        2 => Status::Shlyakhta,
        3 => Status::Kripack,
        _ => unreachable!(),
    };

    let coins = if let Status::Shlyakhta = status {
        50
    } else {
        5
    };

    Some(Character {
        name,
        pronoun,
        status,
        coins,
        events: Vec::new(),
    })
}

fn adventure(
    i18n: &Locale,
    location: &mut Location,
    character: &mut Character,
) -> bool {
    match location {
        Location::City => match choice(i18n, "city-intro", None, 2) {
            1 => {
                *location = Location::Korchma;
                true
            }
            2 => {
                *location = Location::Steppe;
                true
            }
            _ => unreachable!(),
        },
        Location::Korchma => match choice(i18n, "korchma-intro", None, 3) {
            1 => {
                drinker(i18n, character);
                true
            }
            2 => {
                dice(i18n, character);
                true
            }
            3 => {
                *location = Location::City;
                true
            }
            _ => unreachable!(),
        },
        Location::Steppe => false,
    }
}

fn drinker(i18n: &Locale, character: &mut Character) {
    if character.events.contains(&Event::DrunkToDeath) {
        info(i18n, "witness-chance", None);
        return;
    }

    info(i18n, "ask-monster", None);
    info(
        i18n,
        "drinker",
        Some(fluent_args! {
            "name" => &character.name
        }),
    );
    info(
        i18n,
        "coins-info",
        Some(fluent_args! {
            "coins" => character.coins
        }),
    );
    info(i18n, "medovukha-info", None);

    if character.coins >= 1 {
        match choice(i18n, "buying-drink", None, 2) {
            1 => {
                info(i18n, "drunk-to-death", None);
                info(i18n, "sleeping", None);
                info(i18n, "bad-witness", None);

                character.coins -= 1;
                character.events.push(Event::DrunkToDeath);
            }
            2 => info(i18n, "drinker-sad", None),
            _ => unreachable!(),
        }
    } else {
        info(i18n, "cant-afford", None)
    }
}

fn dice(i18n: &Locale, character: &mut Character) {
    struct Game {
        x1: u8,
        x2: u8,
        x3: u8,
        x4: u8,
    }

    impl Game {
        fn throw() -> Self {
            use rand::Rng;

            let mut rng = rand::thread_rng();
            let x1 = rng.gen_range(1..=6);
            let x2 = rng.gen_range(1..=6);
            let x3 = rng.gen_range(1..=6);
            let x4 = rng.gen_range(1..=6);
            Self { x1, x2, x3, x4 }
        }

        fn results(&self, i18n: &Locale) -> u8 {
            // HashMap<ThrowNum, Count>
            let mut results = HashMap::new();

            *results.entry(&self.x1).or_insert(0) += 1;

            *results.entry(&self.x2).or_insert(0) += 1;

            *results.entry(&self.x3).or_insert(0) += 1;

            *results.entry(&self.x4).or_insert(0) += 1;

            for (throw, times) in &results {
                info(
                    i18n,
                    "score",
                    Some(fluent_args! {
                        "times" => times,
                        "num" => *throw
                    }),
                );
            }
            let total = {
                let multiplier: u8 =
                    results.values().copied().max().expect(
                        "at least 4 numbers could be thrown at least once",
                    );
                let sum: u8 = results.keys().map(|throw| **throw).sum();

                sum * multiplier
            };
            info(
                i18n,
                "total",
                Some(fluent_args! {
                    "points" => total
                }),
            );

            total
        }
    }

    if character.coins == 0 {
        info(i18n, "your-empty-purse", None);
        return;
    } else {
        info(
            i18n,
            "coins-info",
            Some(fluent_args! {
                "coins" => character.coins
            }),
        );
    }

    let bet = choice(i18n, "playing-dice", None, character.coins);

    // Game itself
    info(i18n, "dice-rules", None);

    let your_throw = Game::throw();
    info(i18n, "your-results", None);
    let your_total = your_throw.results(i18n);

    let opponent_throw = Game::throw();
    info(i18n, "opponent-results", None);
    let opponent_total = opponent_throw.results(i18n);

    if your_total > opponent_total {
        info(
            i18n,
            "you-won",
            Some(fluent_args! {
                "bet" => bet
            }),
        );
        character.coins += bet;
    } else {
        info(
            i18n,
            "you-lost",
            Some(fluent_args! {
                "bet" => bet
            }),
        );
        character.coins -= bet;
    };

    info(
        i18n,
        "coins-info",
        Some(fluent_args! {
            "coins" => character.coins
        }),
    );
}

fn end(i18n: &Locale, ending: &Ending) {
    match ending {
        Ending::Early => {
            info(i18n, "refuse", None);
        }
        Ending::Unknown => {
            info(i18n, "fear-of-unknown", None);
        }
    }
    println!("{}", i18n.get_msg("end"));
}

enum Location {
    City,
    Steppe,
    Korchma,
}

enum Ending {
    Early,
    Unknown,
}

enum Story {
    Quest,
    Adventure {
        location: Location,
        character: Character,
    },
    End(Ending),
}

struct Game {
    i18n: Locale,
    story: Story,
}

impl Game {
    fn start(i18n: Locale) -> Self {
        Self {
            i18n,
            story: Story::Quest,
        }
    }

    fn step(&mut self) -> bool {
        match &mut self.story {
            Story::Quest => {
                let character = intro(&self.i18n);

                if let Some(character) = character {
                    self.story = Story::Adventure {
                        location: Location::City,
                        character,
                    }
                } else {
                    self.story = Story::End(Ending::Early)
                }

                true
            }
            Story::Adventure {
                location,
                character,
            } => {
                if !adventure(&self.i18n, location, character) {
                    self.story = Story::End(Ending::Unknown);
                }
                true
            }
            Story::End(ending) => {
                end(&self.i18n, ending);
                false
            }
        }
    }
}

pub fn game_loop(i18n: Locale) {
    let mut game = Game::start(i18n);
    while game.step() {}
}
